# Image Classification Using Deep Machine Learning

This project use MATLAB to establish a new system that can identify the images class by recognizing image features and use three different network (google net、Alex net and resnet50) training it separately. 
Finally, the system is established by comparing the accuracy of image recognition and image Classification after training and select the best training net. 

**Introduction**
This part contain all the code of system

**Edition:**
MATLAB 2019

**Datasets:**
Fruit Datasets
[](https://www.kaggle.com/moltean/fruits)


**Comments:**
1、Because the configuration problem of the author's computer, the author did not carry out the experiment with all the fruit data, but selected 10 kinds of fruit with a total of 4000 pictures for the experiment.
2、Because the author is not familiar with the operation of the software, if the document cannot be opened and run correctly, you  copy and paste all the code of the text to get the corresponding program.